/*
 Navicat Premium Data Transfer

 Source Server         : test
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : carrental

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 28/02/2023 21:51:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `carnumber` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '车牌号',
  `type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌',
  `renprice` double NULL DEFAULT NULL COMMENT '租车价格',
  `deposit` double NULL DEFAULT NULL COMMENT '租车押金',
  `peccancy` double NULL DEFAULT NULL COMMENT '违章押金',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '省',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '市',
  `area` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区',
  `jtstore` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '门店',
  `renstatus` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '租车状态0是未租出 1是已租出',
  `carstatus` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tupian` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of car
-- ----------------------------
INSERT INTO `car` VALUES (3, '蒙A.DSB23', 'SUV', '英菲尼迪', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '已租出', '维修中', 'images/yingfeinidi.png');
INSERT INTO `car` VALUES (4, '蒙A.DSB23', '经济型', '腾势', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/tengshi.png');
INSERT INTO `car` VALUES (5, '蒙A.DSB23', '经济型', '一汽马自达', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (7, '蒙A.DSB23', '舒适型', '标致', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/biaozhi.jpg');
INSERT INTO `car` VALUES (8, '蒙A.DSB23', '高端车', '大众斯柯达', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (9, '蒙A.DSB23', '经济型', '英菲尼迪', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '已租出', '维修中', 'images/yingfeinidi.png');
INSERT INTO `car` VALUES (10, '蒙A.DSB23', '经济型', 'MG', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '已租出', '维修中', 'images/mg.jpg');
INSERT INTO `car` VALUES (11, '蒙A.DSB23', '舒适型', '荣威', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '已租出', '维修中', 'images/rongwei.jpg');
INSERT INTO `car` VALUES (12, '蒙A.DSB23', '精英型', '腾势', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/tengshi.png');
INSERT INTO `car` VALUES (13, '蒙A.DSB23', '商务型', '一汽', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (14, '蒙A.DSB23', '电动型', '别克', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/bieke.png');
INSERT INTO `car` VALUES (15, '蒙A.DSB23', 'SUV', '标致', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区1号门店', '未租出', '维修中', 'images/biaozhi.jpg');
INSERT INTO `car` VALUES (16, '蒙A.DSB23', '舒适型', '东风', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/dongfeng.jpg');
INSERT INTO `car` VALUES (17, '蒙A.DSB23', '商务型', '玛莎拉蒂', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/mashaladi.png');
INSERT INTO `car` VALUES (18, '蒙A.DSB23', '电动型', '本田', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/bentian.jpg');
INSERT INTO `car` VALUES (19, '蒙A.DSB23', '高端车', '雪佛兰', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/xuefulan.png');
INSERT INTO `car` VALUES (20, '蒙A.DSB23', '舒适型', '大众', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (21, '蒙A.DSB23', '精英型', '绅宝', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/shenbao.png');
INSERT INTO `car` VALUES (22, '蒙A.DSB23', '精英型', '沃尔沃', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/woerwo.jpg');
INSERT INTO `car` VALUES (23, '蒙A.DSB23', 'SUV', '路虎', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/luhu.jpg');
INSERT INTO `car` VALUES (24, '蒙A.DSB23', 'SUV', '现代', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/xaindai.jpg');
INSERT INTO `car` VALUES (25, '蒙A.DSB23', '经济型', '丰田', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/fengtian.jpg');
INSERT INTO `car` VALUES (26, '蒙A.DSB23', '商务型', '一汽马自达', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区2号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (27, '蒙A.DSB23', '经济型', '起亚', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/qiya.png');
INSERT INTO `car` VALUES (28, '蒙A.DSB23', '舒适型', '福特', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/fute.jpg');
INSERT INTO `car` VALUES (29, '蒙A.DSB23', '电动型', '捷豹', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/jiebao.png');
INSERT INTO `car` VALUES (30, '蒙A.DSB23', '高端车', '奥迪', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/aodi.jpg');
INSERT INTO `car` VALUES (31, '蒙A.DSB23', '精英型', '奔驰', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/benchi.jpg');
INSERT INTO `car` VALUES (32, '蒙A.DSB23', 'SUV', '大众斯柯达', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '新城区', '新城区3号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (33, '蒙A.DSB23', '经济型', '凯迪拉克', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/kasalake.png');
INSERT INTO `car` VALUES (34, '蒙A.DSB23', '舒适型', '马自达', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (35, '蒙A.DSB23', '精英型', 'MG', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/mg.jpg');
INSERT INTO `car` VALUES (36, '蒙A.DSB23', '舒适型', '纳智捷', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/nazhijie.png');
INSERT INTO `car` VALUES (37, '蒙A.DSB23', 'SUV', '玛莎拉蒂', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/mashaladi.png');
INSERT INTO `car` VALUES (38, '蒙A.DSB23', '精英型', '荣威', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/rongwei.jpg');
INSERT INTO `car` VALUES (39, '蒙A.DSB23', '商务型', '大众', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (40, '蒙A.DSB23', '电动型', '一汽马自达', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (41, '蒙A.DSB23', '高端车', '英菲尼迪', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区1号门店', '未租出', '维修中', 'images/yingfeinidi.png');
INSERT INTO `car` VALUES (42, '蒙A.DSB23', 'SUV', '腾势', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/tengshi.png');
INSERT INTO `car` VALUES (43, '蒙A.DSB23', '经济型', '标致', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/biaozhi.jpg');
INSERT INTO `car` VALUES (44, '蒙A.DSB23', '商务型', '大众斯柯达', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (45, '蒙A.DSB23', '舒适型', 'MG', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/mg.jpg');
INSERT INTO `car` VALUES (46, '蒙A.DSB23', '精英型', '荣威', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/rongwei.jpg');
INSERT INTO `car` VALUES (47, '蒙A.DSB23', 'SUV', '一汽', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (48, '蒙A.DSB23', '商务型', '别克', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/bieke.png');
INSERT INTO `car` VALUES (49, '蒙A.DSB23', '电动型', '东风', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/dongfeng.jpg');
INSERT INTO `car` VALUES (50, '蒙A.DSB23', '高端车', '本田', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/bentian.jpg');
INSERT INTO `car` VALUES (51, '蒙A.DSB23', '经济型', '雪佛兰', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/xuefulan.png');
INSERT INTO `car` VALUES (52, '蒙A.DSB23', '舒适型', '绅宝', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/shenbao.png');
INSERT INTO `car` VALUES (53, '蒙A.DSB23', '精英型', '沃尔沃', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/woerwo.jpg');
INSERT INTO `car` VALUES (54, '蒙A.DSB23', 'SUV', '路虎', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区2号门店', '未租出', '维修中', 'images/luhu.jpg');
INSERT INTO `car` VALUES (55, '蒙A.DSB23', '经济型', '现代', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/xaindai.jpg');
INSERT INTO `car` VALUES (56, '蒙A.DSB23', '舒适型', '丰田', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/fengtian.jpg');
INSERT INTO `car` VALUES (57, '蒙A.DSB23', '精英型', '起亚', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/qiya.png');
INSERT INTO `car` VALUES (58, '蒙A.DSB23', 'SUV', '福特', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/fute.jpg');
INSERT INTO `car` VALUES (59, '蒙A.DSB23', '商务型', '捷豹', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/jiebao.png');
INSERT INTO `car` VALUES (60, '蒙A.DSB23', '电动型', '奥迪', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/aodi.jpg');
INSERT INTO `car` VALUES (61, '蒙A.DSB23', '高端车', '奔驰', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '赛罕区', '赛罕区3号门店', '未租出', '维修中', 'images/benchi.jpg');
INSERT INTO `car` VALUES (62, '蒙A.DSB23', '经济型', '凯迪拉克', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/kasalake.png');
INSERT INTO `car` VALUES (63, '蒙A.DSB23', '舒适型', '马自达', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (64, '蒙A.DSB23', '精英型', '纳智捷', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/nazhijie.png');
INSERT INTO `car` VALUES (65, '蒙A.DSB23', 'SUV', '玛莎拉蒂', 200, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/mashaladi.png');
INSERT INTO `car` VALUES (66, '蒙A.DSB23', '商务型', '大众', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (67, '蒙A.DSB23', '电动型', '一汽马自达', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (68, '蒙A.DSB23', '高端车', '英菲尼迪', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/yingfeinidi.png');
INSERT INTO `car` VALUES (69, '蒙A.DSB23', '经济型', '腾势', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/tengshi.png');
INSERT INTO `car` VALUES (70, '蒙A.DSB23', '舒适型', '标致', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区1号门店', '未租出', '维修中', 'images/biaozhi.jpg');
INSERT INTO `car` VALUES (71, '蒙A.DSB23', '精英型', '大众斯柯达', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (72, '蒙A.DSB23', 'SUV', 'MG', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/mg.jpg');
INSERT INTO `car` VALUES (73, '蒙A.DSB23', '经济型', '荣威', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/rongwei.jpg');
INSERT INTO `car` VALUES (74, '蒙A.DSB23', '舒适型', '一汽', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (75, '蒙A.DSB23', '精英型', '别克', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/bieke.png');
INSERT INTO `car` VALUES (76, '蒙A.DSB23', 'SUV', '东风', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/dongfeng.jpg');
INSERT INTO `car` VALUES (77, '蒙A.DSB23', '商务型', '本田', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/bentian.jpg');
INSERT INTO `car` VALUES (78, '蒙A.DSB23', '精英型', '纳智捷', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '可使用', 'images/nazhijie.png');
INSERT INTO `car` VALUES (79, '蒙A.DSB23', '电动型', '玛莎拉蒂', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '可使用', 'images/mashaladi.png');
INSERT INTO `car` VALUES (80, '蒙A.DSB23', '高端车', '大众', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '已租出', 'images/dazhong.jpg');
INSERT INTO `car` VALUES (81, '蒙A.DSB23', '电动型', '雪佛兰', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/xuefulan.png');
INSERT INTO `car` VALUES (82, '蒙A.DSB23', '高端车', '绅宝', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/shenbao.png');
INSERT INTO `car` VALUES (83, '蒙A.DSB23', '经济型', '沃尔沃', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区2号门店', '未租出', '维修中', 'images/woerwo.jpg');
INSERT INTO `car` VALUES (84, '蒙A.DSB23', '舒适型', '路虎', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/luhu.jpg');
INSERT INTO `car` VALUES (85, '蒙A.DSB23', '精英型', '现代', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/xaindai.jpg');
INSERT INTO `car` VALUES (86, '蒙A.DSB23', 'SUV', '丰田', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/fengtian.jpg');
INSERT INTO `car` VALUES (87, '蒙A.DSB23', '商务型', '起亚', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/qiya.png');
INSERT INTO `car` VALUES (88, '蒙A.DSB23', '电动型', '福特', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/fute.jpg');
INSERT INTO `car` VALUES (89, '蒙A.DSB23', '高端车', '捷豹', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/jiebao.png');
INSERT INTO `car` VALUES (90, '蒙A.DSB23', '经济型', '奥迪', 180, 1000, 1000, '内蒙古自治区', '呼和浩特市', '玉泉区', '玉泉区3号门店', '未租出', '维修中', 'images/aodi.jpg');
INSERT INTO `car` VALUES (91, '蒙A.DSB23', '舒适型', '奔驰', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/benchi.jpg');
INSERT INTO `car` VALUES (92, '蒙A.DSB23', '精英型', '凯迪拉克', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/kasalake.png');
INSERT INTO `car` VALUES (93, '蒙A.DSB23', 'SUV', '马自达', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/mazida.jpg');
INSERT INTO `car` VALUES (94, '蒙A.DSB23', '电动型', '宝马', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/baoma.jpg');
INSERT INTO `car` VALUES (95, '蒙A.DSB23', '舒适型', '宝马', 120, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/baoma.jpg');
INSERT INTO `car` VALUES (96, '蒙A.DSB23', '舒适型', '宝马', 150, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/baoma.jpg');
INSERT INTO `car` VALUES (97, '蒙A.DSB23', 'SUV', '宝马', 130, 1000, 1000, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '未租出', '维修中', 'images/baoma.jpg');
INSERT INTO `car` VALUES (98, '蒙A.DSB23', 'SUV', '一汽', 1200, 1200, 12, '内蒙古自治区', '呼和浩特市', '回民区', '回民区店', '已租出', '正常', 'images/1.jpg');
INSERT INTO `car` VALUES (100, NULL, 'SUV', 'MG', 300, 1000, 3000, '内蒙古自治区', '呼和浩特市', '新城区', '', '未租出', '正常', NULL);

-- ----------------------------
-- Table structure for carservice
-- ----------------------------
DROP TABLE IF EXISTS `carservice`;
CREATE TABLE `carservice`  (
  `id` int NOT NULL COMMENT '编号',
  `cid` int NULL DEFAULT NULL COMMENT '车id外键',
  `spendmoney` double NULL DEFAULT NULL COMMENT '维修金额',
  `servicedate` date NULL DEFAULT NULL COMMENT '维修日期',
  `servicemark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cid`(`cid` ASC) USING BTREE,
  CONSTRAINT `cid` FOREIGN KEY (`cid`) REFERENCES `car` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of carservice
-- ----------------------------

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发布内容',
  `releasedate` date NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES (1, '新闻1', '打扫房间萨克雷解放路上课中介费伺服电机酸', NULL);

-- ----------------------------
-- Table structure for renorder
-- ----------------------------
DROP TABLE IF EXISTS `renorder`;
CREATE TABLE `renorder`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int NULL DEFAULT NULL,
  `carid` int NULL DEFAULT NULL,
  `pickdate` datetime NULL DEFAULT NULL COMMENT '取车时间',
  `province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取车门店',
  `city` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `area` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jtstore` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `stilldate` datetime NULL DEFAULT NULL COMMENT '还车时间',
  `renday` int NULL DEFAULT NULL COMMENT '租车天数 归还时间-租车时间',
  `totalprice` double NULL DEFAULT NULL COMMENT '总金额',
  `giveaddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '送车地址',
  `paymethod` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付方式',
  `createtime` datetime NULL DEFAULT NULL COMMENT '订单创建时间',
  `givestatus` int NULL DEFAULT NULL COMMENT '还车状态 0是未还车 1是已还车',
  `realgive` datetime NULL DEFAULT NULL COMMENT '实际还车时间',
  `overdue` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '有无逾期',
  `fine` double(200, 0) NULL DEFAULT NULL COMMENT '罚款',
  `auditing` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核状态',
  `beizhu` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`userid` ASC) USING BTREE,
  INDEX `carid`(`carid` ASC) USING BTREE,
  CONSTRAINT `carid` FOREIGN KEY (`carid`) REFERENCES `car` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `userid` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of renorder
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `identity` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `concat` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `role` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '角色',
  `license` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `uemail` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (2, 'user1', 'user1', '2312312312', '12312312', '用户', 'SE230D', '1299140@qq.com');
INSERT INTO `user` VALUES (3, 'admin', 'admin', '2312312312', '12312312', '管理员', 'SE230D', '1299140@qq.com');

SET FOREIGN_KEY_CHECKS = 1;
