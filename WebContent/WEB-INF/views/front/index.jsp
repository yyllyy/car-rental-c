<%--
  Created by IntelliJ IDEA.
  User: DongYiBin
  Date: 2019/3/18
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>汽车租赁</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/base.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/common.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/rental.css">
</head>
<body>

<div class="container">
    <div class="header">
        <div class="head-bottom">
            <div class="w1180">
                <a href="/" class="logo"></a>
                <div class="nav">
                    <ul class="nav-wrap clear_float" id="J_NavBox">
                       <li class="menu-index menu-index-on"><a href="<%=basePath %>">首页</a><span></span></li>
                        <li class="menu-index"><a href="<%=basePath %>front/notice">公告</a><span></span></li>
                         <li class="menu-index">
                            <a href="<%=basePath %>front/myorder" >我的订单</a><span></span>
                        </li>
                        <li class="menu-index">
                            <a href="<%=basePath %>front/userinfo" >个人信息</a><span></span>
                        </li>
                        <c:if test="${loginUser == null}">
                        <li class="menu-index" style="float: right;">
                           <a href="<%=basePath %>front/toLogin" target="" style="color: black;font-size: 14px;">登录</a><span></span>
                        </li>
                        </c:if>
                       <c:if test="${loginUser != null}">
	                       <li class="menu-index" style="float: right;">
	                           	<i>欢迎您：${loginUser}&ensp;&ensp;</i><span></span>
	                           	<a href="<%=basePath %>front/logout" target="" style="color: black;font-size: 14px;">退出</a>
	                        </li>
                      	</c:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
        <div class="choosecartype">
            <form action="/"  method="post" id="conditionForm">
            	<div class="searcharea" style="background: #f5f5f5;">
            		<!-- 选择门店的 -->
            		<div class="selectstore">
            			<!-- 取车 -->
		                <div class="carstore" >
		                	<!-- 取车门店 -->
							<div id="dist" class="dist" data-toggle="distpicker">
								<label class="key" style="float: left;padding-top: 10px;">取车门店 :</label> 
								<div class="form-group">
	                            	<label class="sr-only" for="province">Province</label>
	                                <select name="province" class="form-control" id="province1" data-province="内蒙古自治区"></select>
	                             </div>
	                             <div class="form-group">
	                                <label class="sr-only" for="city">City</label>
	                                <select name="city" class="form-control" id="city1" data-city="呼和浩特市"></select>
	                             </div>
	                             <div class="form-group" >
	                                 <label class="sr-only" for="area">District</label>
	                                 <select name="area" class="form-control" id="district1" data-district=""></select>
	                             </div>
	                             <div class="form-group" >
	                             	 <select name="jtstore" class="form-control" id="shop">
	                            	 </select>
	                             </div>
		                   	</div>
		                   	<!-- 取车时间 -->
		                   	
						</div>
						<!-- 还车 -->
						<div class="carstore">
							<div class="qutime" >
			                   	<label class="key">取车/还车时间 :</label>
                                <input type="text" class="" id="pickdate" name="pickdate" placeholder="取车/还车时间">
                                <input type="hidden" id="startTime" name="startTime">
                                <input type="hidden" id="endTime" name="endTime">
							</div>
							<%--<div class="qutime" >
			                   	<label class="key">还车时间 :</label>
                                <input type="text" class="" id="stilldate" name="stilldate" placeholder="还车时间">
							</div>--%>
		                   	<!-- 还车时间 -->
		                   	
						</div>
					</div>
					<!-- 查询 -->
					<div class="forsearch">
						<button type="button" class="chaxun">查询</button>
					</div>
				</div>
            </form>
            <div class="car-box-body">
                <div id="token" style="display: none;">
                	<input name="__RequestVerificationToken" type="hidden" value="3bq8YylMIPGmrCG-O7ZgdltgnFtmWV9dzSj2tCLtzau4ZvsFDlhziJOG5-9fPYI-x1WQ_hmJuNKOT79LxjB1uouvG1A1">
                </div>

                <div class="choose-typebox">
                    <div class="choose-model cartypelist">
                        <div id="pagelet-CarLevelList">
                            <a href="javascript:;" class="classify-all on" data-value="">
                                <div class="modelall"><span></span></div>
                            </a>
                            <a href="javascript:;" data-value="经济型">
                                <div class="model-classify">
                                    <p>经济型</p>
                                    <span class="model-pic2"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="舒适型">
                                <div class="model-classify">
                                    <p>舒适型</p>
                                    <span class="model-pic3"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="精英型">
                                <div class="model-classify">
                                    <p>精英型</p>
                                    <span class="model-pic4"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="SUV">
                                <div class="model-classify">
                                    <p>SUV</p>
                                    <span class="model-pic5"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="商务型">
                                <div class="model-classify">
                                    <p>商务型</p>
                                    <span class="model-pic6"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="电动型">
                                <div class="model-classify">
                                    <p>电动型</p>
                                    <span class="model-pic7"></span>
                                </div>
                            </a>
                            <a href="javascript:;" data-value="高端车">
                                <div class="model-classify">
                                    <p>高端车</p>
                                    <span class="model-pic8"></span>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="choose-branch">
                        <div class="choose-brand choose-ck">
                            <span>品 牌：</span>
                            <div class="brandlist">
                                <div id="pagelet-BrandList"><a href="javascript:;" data-value="" class="on"
                                                               data-ba-label="品牌_全部">全部</a>
                                    <a href="javascript:;" data-value="玛莎拉蒂" class="" data-ba-label="品牌_玛莎拉蒂">玛莎拉蒂</a>
                                    <a href="javascript:;" data-value="大众" class="" data-ba-label="品牌_大众">大众</a>
                                    <a href="javascript:;" data-value="一汽马自达" class=""
                                       data-ba-label="品牌_一汽马自达">一汽马自达</a>
                                    <a href="javascript:;" data-value="英菲尼迪" class="" data-ba-label="品牌_英菲尼迪">英菲尼迪</a>
                                    <a href="javascript:;" data-value="腾势" class="" data-ba-label="品牌_腾势">腾势</a>
                                    <a href="javascript:;" data-value="标致" class="" data-ba-label="品牌_标致">标致</a>
                                    <a href="javascript:;" data-value="大众斯柯达" class=""
                                       data-ba-label="品牌_大众斯柯达">大众斯柯达</a>
                                    <a href="javascript:;" data-value="MG" class="" data-ba-label="品牌_MG">MG</a>
                                    <a href="javascript:;" data-value="荣威" class="" data-ba-label="品牌_荣威">荣威</a>
                                    <a href="javascript:;" data-value="一汽" class="" data-ba-label="品牌_一汽">一汽</a>
                                    <a href="javascript:;" data-value="别克" class="" data-ba-label="品牌_别克">别克</a>
                                    <a href="javascript:;" data-value="东风" class="" data-ba-label="品牌_东风">东风</a>
                                    <a href="javascript:;" data-value="本田" class="" data-ba-label="品牌_本田">本田</a>
                                    <a href="javascript:;" data-value="雪佛兰" class="" data-ba-label="品牌_雪佛兰">雪佛兰</a>
                                    <a href="javascript:;" data-value="宝马" class="" data-ba-label="品牌_宝马">宝马</a>
                                    <a href="javascript:;" data-value="上汽大通" class="" data-ba-label="品牌_上汽大通">上汽大通</a>
                                    <a href="javascript:;" data-value="绅宝" class="" data-ba-label="品牌_绅宝">绅宝</a>
                                    <a href="javascript:;" data-value="沃尔沃" class="" data-ba-label="品牌_沃尔沃">沃尔沃</a>
                                    <a href="javascript:;" data-value="路虎" class="" data-ba-label="品牌_路虎">路虎</a>
                                    <a href="javascript:;" data-value="现代" class="" data-ba-label="品牌_现代">现代</a>
                                    <a href="javascript:;" data-value="丰田" class="" data-ba-label="品牌_丰田">丰田</a>
                                    <a href="javascript:;" data-value="起亚" class="" data-ba-label="品牌_起亚">起亚</a>
                                    <a href="javascript:;" data-value="福特" class="" data-ba-label="品牌_福特">福特</a>
                                    <a href="javascript:;" data-value="捷豹" class="" data-ba-label="品牌_捷豹">捷豹</a>
                                    <a href="javascript:;" data-value="日产" class="" data-ba-label="品牌_日产">日产</a>
                                    <a href="javascript:;" data-value="奥迪" class="" data-ba-label="品牌_奥迪">奥迪</a>
                                    <a href="javascript:;" data-value="奔驰" class="" data-ba-label="品牌_奔驰">奔驰</a>
                                    <a href="javascript:;" data-value="凯迪拉克" class="" data-ba-label="品牌_凯迪拉克">凯迪拉克</a>
                                    <a href="javascript:;" data-value="纳智捷" class="" data-ba-label="品牌_纳智捷">纳智捷</a>
                                    <a href="javascript:;" data-value="江淮" class="" data-ba-label="品牌_江淮">江淮</a>
                                    <a href="javascript:;" data-value="江铃" class="" data-ba-label="品牌_江铃">江铃</a>
                                    <a href="javascript:;" data-value="马自达" class="" data-ba-label="品牌_马自达">马自达</a>
                                </div>
                            </div>
                        </div>
                       <!--  <div class="choose-price choose-ck">
                            <span>价 格：</span>
                            <div id="pricelist" class="pricelist">
                                <a href="javascript:;" data-value="" class="on">全部</a>
                                <a href="javascript:;" data-value="A" class="">100元以下</a>
                                <a href="javascript:;" data-value="AA" class="">100-200元</a>
                                <a href="javascript:;" data-value="AAA" class="">200-300元</a>
                                <a href="javascript:;" data-value="AAAA" class="">300元以上</a>

                            </div>
                        </div> -->
                        <div class="choosemore-box" id="J_choosemore">
                            <span class="choose-more"></span>
                        </div>
                    </div>
                </div>
                <!-- <div class="sort clearfix">
                    <ul class="field-orderby" sortby="null">
                        <li class="sort-default">
                            <a href="javascript:;" class="order-def" value="null">默认排序</a>
                        </li>
                        <li class="sort-price">
                            <a href="javascript:;" class="order-def" value="RentDesc">按租金<span class="sort-icon"></span></a>
                        </li>
                        <li class="sort-emission">
                            <a href="javascript:;" class="order-def" value="OutputVolumeDesc">按排量<span
                                    class="sort-icon"></span></a>
                        </li>
                    </ul>
                    <div class="field-level-wrap">
                        <span id="chkIsQueue" style="display: none;">false</span>
                    </div>
                    <div class="field-tip">
                        * 共有<span id="carTypeCount">27</span>种车型可以租用
                    </div>
                </div> -->
                <div class="carwrap clearfix">
                    <div id="imgLoad" class="pageinfo-box" style="display: none;">
                        <p class="loading-box">
                            <img src="/Content/Images/Shared/loading.gif?v=f7b8df4fc7b340e482673b4c78a8d44b" alt="">正在为您查询可租车型...
                        </p>
                    </div>
                    <!-- /loading -->
                    <div id="nocar" style="display: none;">
                        <img src="/Content/Images/Order/Step2/nocar.png?v=f7b8df4fc7b340e482673b4c78a8d44b" alt="">
                        <div class="nocar-explain">
                            <p class="nocar-text1">对不起，当前门店该时间段内车辆已经租满。</p>
                            <p class="nocar-text2">您可以重新选择其他门店。</p>
                        </div>
                    </div>
                    <!-- /nocar -->
                    <div id="carTypeList">
						<div class="wraplist" id="reservationList" data-count="27" data-nostockcount="0">
							<!-- <div class="det-carlist" style="z-index: 299;">
								<ul class="clearfix">
									<li class="licar-pic">
										<a href="javascript:void(0)"
										class="car-pic" data-iscargourp="True" data-cid="564"> 
										<img src="https://files.1hai.cn/group7/M00/3C/02/rBUFH1uI1DeAP0w6AACwGjgs2-o015.jpg?visitType=ext&amp;sign=YzdkZGRhZGUxMTg0NDdiZWEyN2ZlZjc0YzFjNzZhNjA=?v=74b27db818984e05aed99d555a01cc39"
											class="lazy" alt="标致3008">
										</a>
									</li>
									<li class="licar-name">
										<div class="namesub">
											<div class="namecontent">
												<p class="car-nameinfo">
													<span>标致3008或同组车型</span> <a href="javascript:void(0);"
														class="sp-aczs sp-box"><i class="lv5"></i></a> <span
														class="rebate">返</span>
												</p>
												<p class="car-distribute">
													【随机分配如大众斯柯达野帝、Jeep自由侠、标致3008或类似车型】</p>
												<p class="car-introinfo">
													<span>SUV|自动|5座</span>
												</p>
												<p class="car-typeinfo">
													<span></span>
												</p>
											</div>
										</div>
									</li>

									<li class="licar-info">
										<p class="condition1">
											标准价
											<span class="cartitle"> <i
												class="sp-box discount icon-ticket">惠</i>2天日租价
											</span> <span class="carprice"> <i class="symbol">¥</i> <em
												class="total-price">126</em>/日均
											</span> <span class="licar-btn btnopen1 ">预订</span> <span
												class="typebtn btntop1 "> <a href="javascript:;"
												class="type-four type-fourone btn-book" data-price-type="7"
												data-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>
														闪租价<br>
												</span><i class="symbol">¥</i> <em class="total-price">191</em>
											</a> 
											<a href="javascript:;"
												class="type-four type-fourone btn-book" data-price-type="1"
												data-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>
														门店现付<br>
												</span><i class="symbol">¥</i> <em class="total-price">126</em>
											</a> 
											<a href="javascript:;"
												class="type-four type-fourtwo btn-book" data-price-type="5"
												data-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>
														返现价<br> <em>138元返现12元/天</em>
												</span><i class="symbol">¥</i> <em class="total-price">126</em>
											</a> 
											<a href="javascript:;"
												class="type-four type-fourtwo notchoose" data-price-type="3"
												data-cid="4BLP0QpWI10=" data-gid="kQSEOmxNcx0="> <span>
														在线预付<br> <em>需要提前3天预订</em>
												</span><i class="symbol">¥</i> <em class="total-price">124</em>
											</a>
											</span>
										</p>



									</li>
								</ul>
							</div> -->
						</div>

						<div id="moreCarTypeImgLoad" class="pageinfo-box hidden">
                            <p class="loading-box">
                                <img src="/Content/Images/Shared/loading.gif?v=74b27db818984e05aed99d555a01cc39" alt="">正在为您查询可租车型...
                            </p>
                        </div>
                        <!-- <div class="load-more-box">
                            <a href="#1">加载更多车型</a>
                        </div> -->

                        <!-- 页码 -->
                        <div class="text-center" id="pagination"></div>
                       
                    </div>
                    <!-- /车型列表 -->
                </div>
            </div>
        </div>
    </div>

</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>assets/layui/layui.js"></script>
<script src="<%=basePath %>js/distpicker.data.js"></script>
<script src="<%=basePath %>js/distpicker.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/front/index.js"></script>

<script type="text/javascript">
    $(function () {
        // 引入layui的laypage模块
        layui.use(['laypage', 'laydate'], function(){
            var laypage = layui.laypage
              , laydate = layui.laydate;
            // 初始化
            Index.init('<%=basePath %>', laypage, laydate);
        });
    });
</script>
</body>
</html>
