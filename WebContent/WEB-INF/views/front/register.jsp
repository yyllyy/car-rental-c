<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>注册页面</title>
<link rel="stylesheet" href="<%=basePath %>css/front/register.css">
<body>
	<div style="margin-left: 80px; margin-top: 10px;">
		<img alt="" src="../images/logo.png">
	</div>
	<div class="left">
		<img alt="" src="../images/c3.png">
	</div>
	<div class="main1">
		<div class="top">
			注册账号
			<hr>
		</div>
		<div class="bottom">

			<form action="" method="post">
				&nbsp;&nbsp;&nbsp;用户名：<input class="zc" type="text" name="uname"
					required placeholder="请输入用户名"> <span style="color: red;">*</span>
				<br> &nbsp;&nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;码： <input class="zc"
					required type="password" name="upassword" checked="checked"
					placeholder="6-18位，请使用英文字母或数字组合"> <span style="color: red;">*</span><br>
				身份证号：<input class="zc" type="text" name="uid" required
					placeholder="请输入32位身份证号"> <span style="color: red;">*</span>
				<br> 联系方式：<input class="zc" type="text" name="tel" required
					placeholder="请输入11位手机号"> <span style="color: red;">*</span>
				<br>
				驾驶证号：<input class="zc" type="text" name="license" required
					placeholder="请输入32位身份证号"> <span style="color: red;">*</span>
				<br> &nbsp;&nbsp;&nbsp;邮&nbsp;&nbsp;&nbsp;箱：
				<input class="zc" type="text" name="email" required
					placeholder="请输入11位手机号"> <span style="color: red;">*</span>
				<br>
				 <input class="zc" id="register" type="button" value="注册" style="margin-left: 82px;height:35px; background-color: #4a84b3; color: white;"><br>
			</form>

			<span style="color: red; padding-left: 60px; padding-top: 30px;">
			</span>
			<p style="text-align: right; padding-right: 5px;">
				已有账号? <a href="<%=basePath%>front/toLogin">立即登录</a>
			</p>
		</div>
		
	</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	var Register = {
        data: {
            baseUrl: ''
        },
	    init: function (baseUrl) {
            Register.data.baseUrl = baseUrl;
			$('#register').on('click', Register.register);
        },
        getParam: function() {
        	var data = {};
        	data.username =  $("form input[name=uname]").val();
            data.password =  $("form input[name=upassword]").val();
            data.identity =  $("form input[name=uid]").val();
            data.concat =  $("form input[name=concat]").val();
            data.license =  $("form input[name=license]").val();
            data.uemail=  $("form input[name=email]").val();
            data.role='role';
            if (!data.username || data.username.length < 6) {
            	alert("请输入正确用户名");
            	return;
            }
            if (!data.password) {
            	alert("请输入密码");
            	return;
            }
            if (!data.identity) {
            	alert("请输入身份证号码");
            	return;
            }
            if (!data.concat) {
            	alert("请输入驾驶证号码");
            	return;
            }
            if (!data.uemail) {
            	alert("请输入邮箱");
            	return;
            }
           
            return data;
        },
        register: function () {
	        var data = Register.getParam();
	        if (data) {
                $.ajax({
                    url: Register.data.baseUrl + 'user/insert',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function (response) {
                        if (response.type == 'success') {
                        	alert("注册成功");
							window.location.href = Register.data.baseUrl + "front/toLogin";
                        } else {
                            alert(response.message);
						}
                    }
                });
			}

		}
	}
    Register.init('<%=basePath%>');
</script>
</body>
</html>