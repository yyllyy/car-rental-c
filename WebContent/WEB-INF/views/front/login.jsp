<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登录页面</title>
<link rel="stylesheet" href="<%=basePath %>css/front/login.css">
</head>
<body>
	<div style="margin-left: 80px; margin-top: 10px;">
		<img alt="" src="../images/logo2.png">
	</div>
	<div class="left">
		<img alt="" src="../images/c3.png">
	</div>
	<div class="main1">
		<div class="top">
			登录账号
			<hr>
		</div>
		<div class="bottom">
			<form>
				用户名：
				<input id="uname" type="text" name="uname" onblur="Login.checkUserName();" placeholder="请输入用户名"><br>
				<span id="checkUserNameResult" style="color: red; padding-left: 80px; font-size: 12px;"></span> <br>
				密&ensp;&ensp;码：
				<input id="upassword" onblur="Login.checkPassword();" type="password" name="upassword" checked="checked" placeholder="请输入密码"><br>
				<span id="checkPasswordResult" style="color: red; padding-left: 80px; font-size: 12px;"></span><br>
				
			</form>
			<button id="login" style="margin-left: 75px; background-color: #4a84b3; color: white;width: 250px;height: 30px;">登录</button><br>
			<span style="color: red; padding-left: 60px; padding-top: 30px;">
			</span>

		</div>
		<p style="text-align: right; padding-right: 5px;">
			没有账号? <a href="<%=basePath%>/front/toRegister">立即注册</a>
		</p>
	</div>

<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>
	var Login = {
        data: {
            baseUrl: ''
        },
	    init: function (baseUrl) {
            Login.data.baseUrl = baseUrl;
			$('#login').on('click', Login.login);
        },
        checkUserName: function (){
            var uname = $("#uname");
            var checkUserNameResult = $("#checkUserNameResult");
            if(uname.val().trim().length == 0){
                checkUserNameResult.text("用户名不能为空");
                uname.focus();
                return false;
            } else {
                checkUserNameResult.text("");
            }
            return true;
        },
        checkPassword: function (){
            var upassword = $("#upassword");
            var checkPasswordResult = $("#checkPasswordResult");
            if(upassword.val().trim().length == 0){
                checkPasswordResult.text("密码不能为空");
                upassword.focus();
                return false;
            }else{
                checkPasswordResult.text("");
            }
            return true;
        },
		login: function () {
	        if (Login.checkUserName() && Login.checkPassword()) {
	            var data = {};
	            data.username =  $("#uname").val(),
	            data.password =  $("#upassword").val(),
                $.ajax({
                    url: Login.data.baseUrl + 'front/login',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
					// data: data,
                    success: function (response) {
                        if (response.type == 'success') {
                        	if (response.code == 307) {
								window.location.href = Login.data.baseUrl + "admin/index";
                        	} else  {
								window.location.href = Login.data.baseUrl;
                        	}
                        } else {
                            alert(response.message);
						}
                    }
                });
			}

		}
	}
    Login.init('<%=basePath%>');
</script>
</body>
</html>