$(function () {
  Main.init();
});

var Main = {
    data: {
        area: {
            '江苏省': {
                '苏州市': {
                	'虎丘区': ['虎丘区1号门店', '虎丘区2号门店', '虎丘区3号门店'],
                    '吴中区': ['吴中区1号门店', '吴中区2号门店', '吴中区3号门店'],
                    '相城区': ['相城区1号门店', '相城区2号门店', '相城区3号门店'],
                    '姑苏区': ['姑苏区1号门店', '姑苏区2号门店', '姑苏区2号门店'],
                    '吴江区': ['吴江区1号门店', '吴江区2号门店', '吴江区3号门店'],
                    '常熟市': ['常熟市1号门店', '常熟市2号门店'],
                    '张家港市': ['张家港市1号门店', '张家港市2号门店'],
                    '昆山市': ['昆山市1号门店', '昆山市2号门店'],
                    '太仓市': ['太仓市1号门店', '太仓市2号门店'],

                },
                '常州市': {
                    '武进区': ['武进区1号门店', '武进区1号门店', '武进区1号门店'],
                }

            }
        }
    },
    init: function () {
        Main.showTime();
    },
    showTime: function() {
        setInterval(function() {
            var date = new Date();
            var year = date.getFullYear(); //获取当前年份
            var mon = date.getMonth() + 1; //获取当前月份
            var da = date.getDate(); //获取当前日
            var day = date.getDay(); //获取当前星期几
            var h = date.getHours(); //获取小时
            var m = date.getMinutes(); //获取分钟
            var s = date.getSeconds(); //获取秒
            var t= year + '年' + mon + '月' + da + '日' + '星期'
                + day + ' ' + h + ':' + m + ':' + s
            $('#Date').text(t);
        }, 1000)
    },
    // 根据地区加载门店
    showShop: function(a) {
        var province = $('#province1').val();
        var city = $('#city1').val();
        var district = $('#district1').val();
        $('#shop').html(new Option("请选择门店", '', false, true));
        tag : for(var i in Main.data.area) {
            if(province == i) {
                var cityData = Main.data.area[i];
                for(var j in cityData) {
                    if (city == j) {
                        var areaData = cityData[j];
                        for (var k in areaData) {
                            if (district == k) {
                                for (var d in areaData[k]) {
                                    // 选中门店
                                    var seleced = false;
                                    if (a && a == areaData[k][d]) {
                                        seleced = true;
                                    } else if (!a && d == 0) {
                                    	// 是否默认选中门店
//                                        seleced = true;
                                    }
                                    $('#shop').append(new Option(areaData[k][d], areaData[k][d], false, seleced));
                                }
                                break tag;
                            }
                        }
                    }
                }
            }
        }
    },
    // 获取URL中的参数
    getRequest : function() {
		var url = location.search; // 获取url中"?"符后的字串
		var theRequest = new Object();
		if (url.indexOf("?") != -1) {
			var str = url.substr(1);
			strs = str.split("&");
			for (var i = 0; i < strs.length; i++) {
				theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
			}
		}
		return theRequest;
	},
    dateFormat: function () {
        Date.prototype.format = function(format){
            var o = {
                "M+" : this.getMonth()+1, //month
                "d+" : this.getDate(), //day
                "H+" : this.getHours(), //hour
                "m+" : this.getMinutes(), //minute
                "s+" : this.getSeconds(), //second
                "q+" : Math.floor((this.getMonth()+3)/3), //quarter
                "S" : this.getMilliseconds() //millisecond
            }

            if(/(y+)/.test(format)) {
                format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
            }

            for(var k in o) {
                if(new RegExp("("+ k +")").test(format)) {
                    format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
                }
            }
            return format;
        }
    }
}
