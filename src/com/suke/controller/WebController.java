package com.suke.controller;

import com.suke.dto.ResultDto;
import com.suke.pojo.Car;
import com.suke.pojo.User;
import com.suke.service.CarService;
import com.suke.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/front")
public class WebController {
	@Autowired
	private UserService userService;
	
	@Autowired
    private CarService carService;

	@RequestMapping("/index")
	public ModelAndView index(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("/front/index");
		mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
		return mv;
	}

	/**
	 * 退出
	 * @return
	 */
	@RequestMapping("/logout")
	public String logout(HttpServletRequest request) {
		 request.getSession().removeAttribute(request.getSession().getId());
		return "front/login";
	}
	
	/**
	 * 登录页面
	 * @return
	 */
	@RequestMapping("/toLogin")
	public String toLogin() {
		return "front/login";
	}

	/**
	 * 登录
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
	public Object login(HttpServletRequest request, @RequestBody User loginUser) {
		ResultDto resultDto = new ResultDto();
        User user = userService.lookUserByUname(loginUser.getUsername());
        if (user != null) {
        	if ("用户".equals(user.getRole()) || "管理员".equals(user.getRole())) {
        		if (user.getPassword().equals(loginUser.getPassword())) {
        			// 设置session
        			request.getSession().setAttribute(request.getSession().getId(), user.getUsername());
        			resultDto.setMessage("登录成功");
        			resultDto.setCode(200);
        			if("管理员".equals(user.getRole())){
        				resultDto.setCode(307);
                	}
        			return resultDto;
        		}
        	}
        } 
        resultDto.resultError();
        resultDto.setMessage("用户或密码错误");
        resultDto.setCode(200);
		return resultDto;
	}

	/**
	 * 注册页面
	 * @return
	 */
	@RequestMapping("/toRegister")
	public String toRegister() {
		return "front/register";
	}
	
	/**
	 * 预订页面
	 * @return
	 */
	@RequestMapping("/ordercar")
	public ModelAndView toOrder(HttpServletRequest request, int id) {
		ModelAndView mv = new ModelAndView("/front/ordercar");
		Car car = carService.lookCar(id);
        mv.addObject("ordercar", car);
		mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
		return mv;
	}
	/**
	 * 我的订单页面
	 * @return
	 */
	@RequestMapping("/myorder")
	public ModelAndView myOrder(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("/front/myorder");
		mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
		return mv;
	}
	
	/**
	 * 我的订单页面
	 * @return
	 */
	@RequestMapping("/notice")
	public ModelAndView notice(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("/front/notice");
		mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
		return mv;
	}
	
	/**
	 * 我的信息
	 * @return
	 */
	@RequestMapping("/userinfo")
	public ModelAndView userInfo(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("/front/userinfo");
		String username= (String) request.getSession().getAttribute(request.getSession().getId());
		User user=userService.lookUserByUname(username);
		mv.addObject("userinfo",user);
		mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
		return mv;
	}
	/**
	 * 修改信息
	 */
	@RequestMapping("/modifyuserinfo")
	public ModelAndView modifyUserInfo(HttpServletRequest request,User user) {
		userService.updateUser(user);
		return userInfo(request);
	}
	 /**
     * 修改密码
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/modifypwd", method = RequestMethod.POST)
    public ModelAndView modifypwd(HttpServletRequest request,User user) {
        ModelAndView mv = new ModelAndView("front/userinfo");
        userService.modifypwd(user);
		User user2=userService.lookUser(user.getId());
		mv.addObject("userinfo",user2);
        mv.addObject("msg","修改成功");
        mv.addObject("loginUser", request.getSession().getAttribute(request.getSession().getId()));
        return mv;
    }


}
