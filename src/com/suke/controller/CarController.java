package com.suke.controller;


import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.dto.ResultDto;
import com.suke.pojo.Car;
import com.suke.service.CarService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 汽车管理
 *
 * @author wuxueli
 */
@Controller
@RequestMapping("/car")
public class CarController {
    @Autowired
    private CarService carService;

    /**
     * 首页
     * @author wuxueli
     * @return
     */
    @RequestMapping("/index")
    public String toIndex() {
        return "admin/index";
    }

    /**
     * 用户租车首页无条件和条件查询
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/userPage", method = RequestMethod.POST)
    @ResponseBody
    public Object userPage(@RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        PageDto page = carService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }
    
    /**
     * 汽车管理界面
     * @author wuxueli
     * @return
     */
    @RequestMapping("/list")
    public String carList() {
        return "admin/car/carlist";
    }

    /**
     * 查询所有
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    public Object page(@RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        PageDto page = carService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 编辑
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/look")
    public ModelAndView lookCar(Integer id) {
        ModelAndView view = new ModelAndView();
        Car car = carService.lookCar(id);
        view.addObject("look", car);
        view.setViewName("admin/car/carlook");
        return view;
    }

    /**
     * 删除
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ResultDto deleteCar(Integer id) {
        carService.deleteCar(id);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("删除成功");
        resultDto.setCode(200);
        return resultDto;
    }
    /**
     * 汽车添加页面
     * @return
     */
    @RequestMapping("/toAdd")
    public String carAdd() {
        return "admin/car/caradd";
    }

    /**
     * 添加
     * @param car
     * @return
     */
    @RequestMapping(value="/insert", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto insertCar(@RequestBody Car car) {
        carService.insertCar(car);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("添加成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 更新
     * @param car
     * @return
     */
    @RequestMapping(value="/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateCar(@RequestBody Car car) {
        carService.updateCar(car);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("保存成功");
        resultDto.setCode(200);
        return resultDto;
    }
}
