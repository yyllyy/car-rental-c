package com.suke.controller;

import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.dto.ResultDto;
import com.suke.pojo.User;
import com.suke.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @params
 * @author wuxueli
 * @return
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

    /**
     * 用户添加页面
     * @return
     */
    @RequestMapping("/toAdd")
    public String userAdd() {
        return "admin/user/useradd";
    }
    /**
     * 用户密码修改
     * @return
     */
    @RequestMapping("/modifypwd")
    public String modifypwd() {
        return "admin/user/modifypwd";
    }
    
    /**
     * 添加
     * @param user
     * @return
     */
    @RequestMapping(value="/insert", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto insertCar(@RequestBody User user) {
    	user.setRole("用户");
        userService.insertUser(user);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("添加成功");
        resultDto.setCode(200);
        return resultDto;
    }
    /**
     * 用户管理界面
     * @author wuxueli
     * @return
     */
    @RequestMapping("/list")
    public String userList() {
        return "admin/user/userlist";
    }


    /**
     * 查询所有
     * @param dataGridDto
     * @author wuxueli
     */
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    @ResponseBody
    public Object page(@RequestBody DataGridDto dataGridDto) {
        ResultDto resultDto = new ResultDto();
        PageDto page = userService.selectByCondition(dataGridDto);
        resultDto.setData(page);
        resultDto.setMessage("查询成功");
        resultDto.setCode(200);
        return resultDto;
    }
    
    /**
     * 删除
     *
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    public ResultDto deleteCar(Integer id) {
    	userService.deleteUser(id);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("删除成功");
        resultDto.setCode(200);
        return resultDto;
    }

    /**
     * 编辑用户
     * @param id
     * @author wuxueli
     */
    @RequestMapping(value = "/look", method = RequestMethod.GET)
    public ModelAndView userLook(Integer id) {
        ModelAndView mv = new ModelAndView("admin/user/userlook");
        User user = userService.lookUser(id);
        mv.addObject("look", user);
        return mv;
    }

    /**
     * 更新
     * @param user
     * @return
     */
    @RequestMapping(value="/update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto updateCar(@RequestBody User user) {
        userService.updateUser(user);
        ResultDto resultDto = new ResultDto();
        resultDto.setMessage("保存成功");
        resultDto.setCode(200);
        return resultDto;
    }
   
   


}
