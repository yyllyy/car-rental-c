package com.suke.service;

import com.suke.dao.RenorderMapper;
import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.Renorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RenorderService {
	@Autowired
	private RenorderMapper renorderMapper;

	/**
     * 条件查询
     *
     * @param
     * @return
     * @author dyb
     */
    public PageDto<Renorder> selectByCondition(DataGridDto dataGridDto) {
        PageDto<Renorder> page = new PageDto<>();
        List<Renorder> list = renorderMapper.selectByCondition(dataGridDto);
        int count = renorderMapper.countByCondition(dataGridDto);
        CountFine(list);
        page.setPageSize(dataGridDto.getLineSize());
        page.setTotalDataCount(count);
        page.setData(list);
        return page;
    }
    
    /**
     * 计算罚金
     * @param list
     */
    private void CountFine(List<Renorder> list) {
    	for (Renorder o : list) {
    		if (o.getAuditing().equals("审核通过") && o.getGivestatus() != null && o.getGivestatus() == 0) {
    			long dateTime = System.currentTimeMillis() - o.getStilldate().getTime();
    			if (dateTime > 0) {
    				double f = Math.ceil(dateTime / (1000 * 60 * 60)) * 12 ;
    				o.setFine(f);
    				renorderMapper.update(o);
    			}
    		}
    	}
    }
    
    /**
     * 删除
     * @author dyb
     * @param id
     */
    public void delete(Integer id) {
        renorderMapper.delete(id);
    }

    /**
     * 添加
     * @param renorder
     * @author dyb
     */
    public void insert(Renorder renorder) {
        renorderMapper.insert(renorder);
    }

    /**
     * 编辑第一步
     *
     * @param id
     * @author dyb
     */
    public Renorder select(Integer id) {
        return renorderMapper.select(id);
    }

    public void update(Renorder renorder) {
        renorderMapper.update(renorder);
    }

}
