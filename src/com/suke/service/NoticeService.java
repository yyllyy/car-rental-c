package com.suke.service;

import com.suke.dao.NoticeMapper;
import com.suke.dto.DataGridDto;
import com.suke.dto.PageDto;
import com.suke.pojo.Notice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NoticeService {
    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 条件查询
     *
     * @param
     * @return
     * @author dyb
     */
    public PageDto<Notice> selectByCondition(DataGridDto dataGridDto) {
        PageDto<Notice> page = new PageDto<>();
        List<Notice> list = noticeMapper.selectByCondition(dataGridDto);
        int count = noticeMapper.countByCondition(dataGridDto);
        page.setPageSize(dataGridDto.getLineSize());
        page.setTotalDataCount(count);
        page.setData(list);
        return page;
    }

    /**
     * 编辑第一步
     *
     * @param id
     * @author dyb
     */
    public Notice select (Integer id) {
        return noticeMapper.select(id);
    }

    /**
     * 删除
     * @author dyb
     * @param id
     */
    public void delete(Integer id) {
        noticeMapper.delete(id);
    }

    /**
     * 添加
     * @param notice
     */
    public void insert(Notice notice) {
    	noticeMapper.insert(notice);
    }

    /**
     * 更新
     * @param notice
     */
    public void update(Notice notice) {
    	noticeMapper.update(notice);
    }


}
