package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.Notice;

import java.util.List;

public interface NoticeMapper {
	
	/**
	 * 条件查询
	 * @author dyb
	 * @param dataGridDto 分页条件
	 * @return list
	 */
	List<Notice> selectByCondition(DataGridDto dataGridDto);

	Integer countByCondition(DataGridDto dataGridDto);
	
	//编辑
	Notice select(Integer id);
	//删除
	void delete(Integer id);
	//添加
	void insert(Notice notice) ;
	//更新
	void update(Notice notice) ;

}
