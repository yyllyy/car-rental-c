package com.suke.dao;

import com.suke.dto.DataGridDto;
import com.suke.pojo.Renorder;

import java.util.List;

public interface RenorderMapper {
	/**
	 * 条件查询
	 * @author dyb
	 * @param dataGridDto 分页条件
	 * @return list
	 */
	List<Renorder> selectByCondition(DataGridDto dataGridDto);

	Integer countByCondition(DataGridDto dataGridDto);
	
	void delete(Integer id);

	void update(Renorder renorder);

	void insert(Renorder renorder);

	Renorder select(Integer id);

}
