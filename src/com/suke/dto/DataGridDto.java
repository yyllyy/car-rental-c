package com.suke.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/***
 * 分页面参数
 * @author dyb
 */
public class DataGridDto implements Serializable {
    /**
     * 分页limit的开始位置
     * @author dyb
     */
    private int start;
    /**
     * 没页最后一项的下标
     * @author dyb
     */
    private int end;
    /**
     * 分页的当前页数
     * 前台传入
     */
    private int pageIndex = 1;
    /**
     * 每页行数
     * 可由前台传入
     * @author dyb
     */
    private int lineSize = 10;
    private Map<String, Object> params = new HashMap<>(0);

    public int getStart() {
        return (getPageIndex() -1) * lineSize;
    }

    public int getEnd() {
        return getStart() + getLineSize();
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getLineSize() {
        return lineSize;
    }

    public void setLineSize(int lineSize) {
        this.lineSize = lineSize;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
