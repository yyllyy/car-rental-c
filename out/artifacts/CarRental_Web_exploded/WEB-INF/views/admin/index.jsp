<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <title>首页</title>
    <style type="text/css">
        .header a {
            color: white;
        }
        
        .right_content{
        	float: right;
        }
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"
                   style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
            <div>

                <p class="navbar-text navbar-left">
                    当前时间： <span id="Date"></span>
                </p>
                <!--向右对齐-->
                <p class="navbar-text navbar-right" style="margin-right: 100px;">
                    <a href="<%=basePath %>front/logout">退出</a>
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="#" class="dropdown-toggle"
                                            data-toggle="dropdown"> admin <b class="caret"></b>
                    </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">jmeter</a></li>
                            
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

	<!-- 左边查询框 -->
	<div class="left col-lg-2 col-md-2 col-xs-2">
		<ul class="nav nav-pills nav-stacked">
			<li class="active"><a href="<%=basePath%>admin/index">首页</a></li>
			<li><a href="<%=basePath%>car/list">汽车管理</a></li>
			<li><a href="<%=basePath%>order/list">订单管理</a></li>
			<li><a href="<%=basePath%>user/list">用户管理</a></li>
<!-- 			<li><a href="#">维修管理</a></li> -->
			<li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%-- 			 <li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
		</ul>
	</div>
	   
	    
	<!-- 右边内容部门 -->
 	<div class="right_content" style="border: 1px solid #ccc;height: 595px;width: 1110px; text-align: center;font-style: italic;">
 		<h2 style="margin-top: 200px;">欢迎使用汽车租赁管理系统！</h2>
 	</div>

 

<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../js/main.js"></script>
<script type="text/javascript">

</script>
</body>
</html>