<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/main.css">
    <title>用户管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"
                   style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
            <div>

                <p class="navbar-text navbar-left">
                    当前时间： <span id="Date"></span>
                </p>

                <!--向右对齐-->
                <p class="navbar-text navbar-right" style="margin-right: 100px;">
                    <a href="<%=basePath %>front/logout">退出</a>
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">jmeter</a></li>
                           
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
                <li><a href="<%=basePath%>admin/index">首页</a></li>
                <li><a href="<%=basePath%>car/list">汽车管理</a></li>
                <li class="active"><a href="<%=basePath%>order/list">订单管理</a></li>
                <li><a href="<%=basePath%>user/list">用户管理</a></li>
<!--                 <li><a href="#">维修管理</a></li> -->
                <li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%--                 <li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10">
	        <div class="search">
	            <form role="form" id="conditionForm">
	             	审核状态:<select name="auditing" >
	             		<option value="">请选择</option>
		            	<option value="待审核">待审核</option>
		            	<option value="审核通过">审核通过</option>
	           		 </select>
                    <button type="button" class="select">查询</button>
	            </form>
	        </div>
<!-- 	        <div style="height: 30px; background: #f5f5f5; padding-top: 5px; padding-left: 10px;"> -->
<!-- 	            	排序： <a style="">按标题</a> &nbsp;&nbsp; <a>按日期</a> -->
<!-- 	        </div> -->
	        <div style="padding-left: 10px;">
	            <table class="table table-hover">
	                <caption></caption>
	                <thead>
	                    <tr>
	                        <th>id</th>
                            <th>用户名</th>
                            <th>汽车品牌</th>
                            <th>租车价格</th>
                            <th>租车天数</th>
                            <th>押金</th>
                            <th>订单总额</th>
                            <th>支付方式</th>
                            <th>还车状态</th>
                            <th>创建时间</th>
                            <th>审核状态</th>
                            <th>操作</th>
	                    </tr>
	                </thead>
	                <tbody id="orderList"></tbody>
	            </table>
	        </div>
	        <div class="text-center" id="pagination"></div>
    	</div>
	</div>
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>assets/layui/layui.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/order/list.js"></script>
<script type="text/javascript">
    $(function () {
        // 引入layui的laypage模块
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            // 初始化
            OrderList.init('<%=basePath %>', laypage);
        });
    });
</script>
</body>
</html>