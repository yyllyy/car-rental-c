<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if IE]>
    <script src="http://libs.baidu.com/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/main.css">
    <title>汽车管理</title>
    <style type="text/css">
        .header a {
            color: white;
        }
    </style>
</head>
<body>
<div class="header" style="height: 55px;">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-inner" style="">
            <div class="navbar-header">
                <a class="navbar-brand" href="#" style="margin-right: 50px; margin-left: 20px;">车辆租赁系统</a>
            </div>
            <div>
                <p class="navbar-text navbar-left">
                   	 当前时间： <span id="Date"></span>
                </p>
                <!--向右对齐-->
                <p class="navbar-text navbar-right" style="margin-right: 100px;">
                    <a href="<%=basePath %>front/logout">退出</a>
                </p>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown"> admin <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">jmeter</a></li>
                            
                        </ul>
                    </li>
                </ul>
                <!-- <form class="navbar-form navbar-right" role="search"> -->
                <!-- 	<button type="submit" class="btn btn-default">向右对齐-提交按钮</button> -->
                <!-- </form> -->
            </div>
        </div>
    </nav>
</div>
<div class="container-fluid">
    <div class="row">
        <!-- 左边查询框 -->
        <div class="left col-lg-2 col-md-2 col-xs-2">
            <ul class="nav nav-pills nav-stacked">
				<li><a href="<%=basePath%>admin/index">首页</a></li>
                <li class="active"><a href="<%=basePath%>car/list">汽车管理</a></li>
				<li><a href="<%=basePath%>order/list">订单管理</a></li>
                <li><a href="<%=basePath%>user/list">用户管理</a></li>
                <li><a href="#">维修管理</a></li>
				<li><a href="<%=basePath%>notice/list">公告管理</a></li>
<%-- 				<li><a href="<%=basePath%>user/modifypwd">修改密码</a></li> --%>
            </ul>
        </div>
        <!-- 右边内容部门 -->
        <div class="right col-lg-10 col-md-10 col-xs-10">
	        <div class="looktable">
	            <form id="carForm" method="post" class="form-inline">
	                <table>
	                    <tr>
	                        <td class="zuotd" colspan="2" style="text-align: center;">信息修改</td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">编号:</td>
	                        <td class="youtd"><input type="text" name="id" value="${look.id }" disabled></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">类型:</td>
	                        <td class="youtd"><input type="text" name="type" value="${look.type }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">品牌:</td>
	                        <td class="youtd"><input type="text" name="brand" value="${look.brand }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">租价:</td>
	                        <td class="youtd"><input type="text" name="renprice" value="${look.renprice }">/天</td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">押金:</td>
	                        <td class="youtd"><input type="text" name="deposit" value="${look.deposit }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">违章押金:</td>
	                        <td class="youtd"><input type="text" name="peccancy" value="${look.peccancy }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">所属地区:</td>
	                        <td class="youtd">
	                            <div data-toggle="distpicker">
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="province">Province</label>
	                                    <select name="province" class="form-control" id="province1" data-province="${look.province}"></select>
	                                </div>
	                                </br>
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="city">City</label>
	                                    <select name="city" class="form-control" id="city1" data-city="${look.city}"></select>
	                                </div>
	                                </br>
	                                <div class="form-group" style="padding-top: 2px; width: 174px;">
	                                    <label class="sr-only" for="area">District</label>
	                                    <select name="area" class="form-control" id="district1" data-district="${look.area}"></select>
	                                </div>
	                            </div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">所属门店:</td>
	                        <td class="youtd">
	                            <select name="jtstore" class="form-control" id="shop">
	                                <option value="${look.jtstore}">${look.jtstore}</option>
	                            </select>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">租车状态:</td>
	                        <td class="youtd"><input type="text" name="renstatus" value="${look.renstatus }"></td>
	                    </tr>
	                    <tr>
	                        <td class="zuotd">车辆状态:</td>
	                        <td class="youtd"><input type="text" name="carstatus" value="${look.carstatus }"></td>
	                    </tr>
	                    <tr>
	                        <td colspan="2" style="text-align: center;">
								<br>
								<a href="#" class="btn btn-primary update" role="button">保 存</a>
								<a href="<%=basePath%>car/list" class="btn btn-default" role="button">返 回</a>
	                        </td>
	                    </tr>
	                </table>
	            </form>
	        </div>
    	</div>
	</div>
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../js/distpicker.data.js"></script>
<script src="../js/distpicker.js"></script>
<script src="../js/main.js"></script>
<script src="../js/carlook.js"></script>
<script type="text/javascript">
    $(function () {
        CarLook.init('<%=basePath%>', '${look.jtstore}');
    });
</script>
</body>
</html>