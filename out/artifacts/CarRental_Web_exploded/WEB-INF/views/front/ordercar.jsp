<%--
  Created by IntelliJ IDEA.
  User: DongYiBin
  Date: 2019/3/18
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>汽车预订</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/base.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/common.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/rental.css">
    
    <link rel="stylesheet" href="<%=basePath %>css/front/ordercar.css">
</head>
<body>

<div class="container">
	<!-- 导航栏部分 -->
    <div class="header">
        <div class="head-bottom">
            <div class="w1180">
                <a href="/" class="logo"></a>
                <div class="nav">
                    <ul class="nav-wrap clear_float" id="J_NavBox">
                        <li><a href="/" target="">首页</a><span></span></li>
                       
                         
                        
	                       <li class="menu-index" style="float: right;">
	                           	<i>欢迎您：${loginUser}&ensp;&ensp;</i><span></span>
	                           	<a href="<%=basePath %>front/logout" target="" style="color: black;font-size: 14px;">退出</a><span></span
	                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- 预订信息部分 -->
    <div class="orderpart" style="">
    	<!-- 左边信息部分 -->
    	<div class="left">
    		<div class="left-top">
    			<div class="left-img" >
    				<img alt="" src="<%=basePath%>images/benchi.jpg"><br>
					<p style="text-align: center;">租期：<span>0</span>天</p>
    			</div>
    			<div class="left-msg">
    				<div class="left-msg-title">${ordercar.brand }3008或同组车型</div>
    				<div class="left-msg-content">
    					<span style="font-weight: bold;font-size: 14px;">取车信息：</span><br>
    					<span class="pickdate">取车时间：<span>-</span></span><br>
    					<span class="stilldate">还车时间：<span>-</span></span><br>
    					<span>取车门店：${ordercar.province }${ordercar.city }${ordercar.area }${ordercar.jtstore }</span><br>
    					
    					<span style="font-weight: bold;font-size: 14px;">订单说明：</span><br>
    					<span>不限公里数，超时费按车辆租赁费及门店服务费均价÷6收取实际超期小时费 (部分0元活动订单，按照40元/小时进行收取)。</span>
    				</div>
    				
    			</div>
    		</div>
    		<div class="left-bottom">
    			<div class="title">支付方式</div>
    			<div class="payment-list">
	    			<ul class="pay-ul" >
	    				<li>
	    					<label>
	    						<input name="pay" type="radio" class="" checked> &nbsp;&nbsp;门店支付
	    					</label>
	    				</li>
	    				<li>
	    					<label>
	    						<input name="pay" type="radio" class=""> &nbsp;&nbsp;在线支付
	    					</label>
	    				</li>
	    			</ul>
    			</div>
    		</div>
    	</div>
    	<!-- 右边支付金额信息 -->
    	<div class="right">
    		<div class="right-title">
    			费用明细
    		</div>
    		<ul style="padding-left: 10px;padding-right:10px;line-height: 60px;font-size: 14px;">
    			<li>车辆租赁费  <span class="renprice">${ ordercar.renprice}</span></li>
    			<li>服务费  <span class="serviceprice">20</span></li>
    			<li>车辆押金（可退） <span class="deposit">${ ordercar.deposit}</span></li>
    			<li style="font-size: 18px;">总计：<span class="total" style="font-size: 25px;">0</span></li>
    			<!-- 通过js绑定事件，在ordercar。js中 -->
    			<input type="button" name="commitOrder" value="提交订单">
    			
    		</ul>
    		
    	</div>
    </div>
        

</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>js/distpicker.data.js"></script>
<script src="<%=basePath %>js/distpicker.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/front/ordercar.js"></script>
<script type="text/javascript">
    $(function () {
        // 初始化
        Order.init('<%=basePath%>');
	});
</script>
</body>
</html>
