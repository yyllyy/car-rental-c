<%--
  Created by IntelliJ IDEA.
  User: DongYiBin
  Date: 2019/3/18
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>个人中心</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/base.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/common.css">
    
    <link rel="stylesheet" href="<%=basePath %>css/front/userinfo.css">
</head>
<body>

<div class="container">
	<!-- 导航栏部分 -->
    <div class="header">
        <div class="head-bottom">
            <div class="w1180">
                <a href="/" class="logo"></a>
                <div class="nav">
                    <ul class="nav-wrap clear_float" id="J_NavBox">
                        <li class="menu-index"><a href="<%=basePath %>" >首页</a><span></span></li>
                        <li class="menu-index"><a href="<%=basePath %>front/notice" >公告</a><span></span></li>
                         <li class="menu-index">
                            <a href="<%=basePath %>front/myorder" >我的订单</a><span></span>
                        </li>
                        <li class="menu-index menu-index-on">
                            <a href="<%=basePath %>front/userinfo" >个人信息</a><span></span>
                        </li>
                        
                      
                       <li class="menu-index" style="float: right;">
                           	<i>欢迎您：${loginUser}&ensp;&ensp;</i><span></span>
                           	<a href="<%=basePath %>front/logout" target="" style="color: black;font-size: 14px;">退出</a><span></span
                        </li>
                      	
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- 我的订单信息部分 -->
    <div class="dingdan" style="">
    	<div class="dingdan-title"   style="color: #eb5b00;">
    		个人中心
    		<div style="float: right;margin-right: 100px;">
    			<span id="modifypwd">修改密码</span>
	    		<span id="baseinfo"  style="color: black;"> 基本信息</span>
				
    		</div>
    	</div>	
    	<!--基本信息 -->
    	<div class="jiben" >
    		<form action="<%=basePath %>front/modifyuserinfo" method="post">
    			<table>
	    			<tr>
	    				<input type="hidden" name="id" value="${userinfo.id }"/>
	    				<td class="lefttd">用户名：</td>
	    				<td class="righttd"><input type="text" name="username" value="${userinfo.username }"/></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">证件类型：</td>
	    				<td class="righttd">大陆身份证</td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">身份证号：</td>
	    				<td class="righttd"><input type="text" name="identity" value="${userinfo.identity }"/></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">联系方式：</td>
	    				<td class="righttd"><input type="text" name="concat" value="${userinfo.concat}"/></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">电子邮件：</td>
	    				<td class="righttd"><input type="text" name="uemail" value="${userinfo.uemail}"/></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">驾驶证：</td>
	    				<td class="righttd"><input type="text" name="license" value="${userinfo.license}"/></td>
	    			</tr>
	    			<tr>
	    				<td class="righttd" colspan=2 style="text-align: center;">
	    					<button type="submit" style="background: #ca6600;color: white;width: 272px;height:38px;border: 1px solid #ccc;">修改</button>
	    				</td>
	    			</tr>
    			</table>
    		</form>
    		
    	</div>
    	<!--修改密码 -->
    	<div class="modifypwd">
    		<form action="<%=basePath %>front/modifypwd" method="post">
	    		<table>
	    			<tr>
	    				<td class="lefttd">原密码：</td>
	    				<td class="righttd">
	    					<input type="hidden"  name="id" value="${userinfo.id }">
	    					<input id="hidepwd" type="hidden"  value="${userinfo.password }">
							<input id="oldpwd" type="password" name="oldpassword" placeholder="请输入原始密码" required="required"><span style="color:red;">*</span>
						</td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">新密码：</td>
	    				<td class="righttd"><input id="newpwd" type="password" name="password" placeholder="请输入新密码" required="required"><span style="color:red;">*</span></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd">确认密码：</td>
	    				<td class="righttd"><input id="quepwd" type="password" name="quepassword" placeholder="请再次输入新密码" required="required"><span style="color:red;">*</span></td>
	    			</tr>
	    			<tr>
	    				<td class="lefttd"></td>
	    				<td class="righttd"><input id="affirm"  type="submit" value="确认修改" style="background: #ca6600;color: white;"></td>
	    			</tr>
	    			
	    			
	    		</table>
	    		<%-- <div id="msg" style="text-align: center; color: red;">${msg }</div> --%>
    		</form>
    	</div>
    </div>
    
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>assets/layui/layui.js"></script>
<script src="<%=basePath %>js/distpicker.data.js"></script>
<script src="<%=basePath %>js/distpicker.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/front/userinfo.js"></script>

<script>

</script>

<script type="text/javascript">
    $(function () {
        // 引入layui的laypage模块
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            // 初始化
            Index.init('<%=basePath %>', laypage);
        });
    });
</script>
</body>
</html>
