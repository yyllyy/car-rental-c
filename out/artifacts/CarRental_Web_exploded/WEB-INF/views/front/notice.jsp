<%--
  Created by IntelliJ IDEA.
  User: DongYiBin
  Date: 2019/3/18
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
%>
<html>
<head>
    <title>公告</title>
    <link rel="stylesheet" href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-datepaginator/1.1.0/bootstrap-datepaginator.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="<%=basePath %>assets/layui/css/layui.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/base.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/common.css">
    <link rel="stylesheet" href="<%=basePath %>css/front/myorder.css">
    <style>
    	a.on{
    		color: #23b7b7;
    	}
    </style>
</head>
<body>

<div class="container">
	<!-- 导航栏部分 -->
    <div class="header">
        <div class="head-bottom">
            <div class="w1180">
                <a href="/" class="logo"></a>
                <div class="nav">
                    <ul class="nav-wrap clear_float" id="J_NavBox">
                        <li class="menu-index"><a href="<%=basePath %>" >首页</a><span></span></li>
                        <li class="menu-index menu-index-on"><a href="#" >公告</a><span></span></li>
                        <li class="menu-index">
                            <a href="<%=basePath %>front/myorder" >我的订单</a><span></span>
                        </li>
                        <li class="menu-index">
                            <a href="<%=basePath %>front/userinfo" >个人信息</a><span></span>
                        </li>
                        
	                       <li class="menu-index" style="float: right;">
	                           	<i>欢迎您：${loginUser}&ensp;&ensp;</i><span></span>
	                           	<a href="<%=basePath %>front/logout" target="" style="color: black;font-size: 14px;">退出</a><span></span
	                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- 我的订单信息部分 -->
    <div class="dingdan" style="">
    	<div class="dingdan-title"   style="color: #eb5b00;">
    		新闻公告
    		<div style="float: right;margin-right: 100px;">
	    		<span><a href="#all" class="on" data-value='all'>全部公告</a></span>
    		</div>
    	</div>
    	<div class="table-responsive" style="font-size: 14px;">
			<table class="table" style="font-size: 14px;padding-left: 10px;">
				<caption></caption>
				<thead>
					<tr>
						<th>标题</th>
						<th>简介</th>
						<th>发布日期</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody id="noticeList">
				</tbody>
			</table>
		</div>  	
    	<div class="text-center" id="pagination"></div>
	
    </div>
<div class="modal" id="mymodal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="modal-title">模态弹出窗标题</h4>
            </div>
            <div class="modal-body" id="modal-body" style="font-size: 14px;line-height: 2.5em;">
                <p>模态弹出窗主体内容</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    
</div>
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<%=basePath %>assets/layui/layui.js"></script>
<script src="<%=basePath %>js/main.js"></script>
<script src="<%=basePath %>js/front/notice.js"></script>

<script type="text/javascript">
    $(function () {
        // 引入layui的laypage模块
        layui.use('laypage', function(){
            var laypage = layui.laypage;
            // 初始化
            Notice.init('<%=basePath %>', laypage);
        });
    });
</script>
</body>
</html>
