var laypage;
var Notice = {
	data: {
		baseUrl: '',
		lineSize: 8,
		totalPage: 1,
        totalData: 1,
	},
	init: function(baseUrl, a) {
        laypage = a;
        Main.dateFormat();
        Notice.data.baseUrl = baseUrl;
        Notice.page(1);
		// 绑定click事件
		$('#noticeList').on('click', '.look', Notice.look);
	},
    select: function () {
    	Notice.page(1);
    },
	// 分页查询
	page: function (pageIndex) {
		// 获取查询条件
        // 初始化data
		var data={
            pageIndex: pageIndex,
            lineSize: Notice.data.lineSize,
		};
		// ajax post
		$.ajax({
			url: Notice.data.baseUrl + 'notice/page',
			type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
			data: JSON.stringify(data),
            success: function (response) {
				if(response.type == 'success') {
                    var page = response.data;
                    // 同步总页数
                    Notice.data.totalPage = page.totalPageCount;
                    // 总条数
					Notice.data.totalData = page.totalDataCount;
					var data = page.data;
					var NoticeHtml = '';
					$.each(data, function (index, item) {
                        NoticeHtml += '<tr>' +
                        	'<th>'+ item.title +'</th>' +
    						'<th>'+ ((item.content&&item.content.length>20)? item.content.substr(0, 20)+'...': item.content) +'</th>' +
    						'<th>'+ new Date(item.releasedate).format('yyyy-MM-dd') +'</th>' +
    						'<th data-value="'+ item.content +'"><span class="btn btn-sm btn-default look btn btn-primary btn-lg">查看详情</span>' + 
    						'</th>' +
    					'</tr>';
                    });
					$('#noticeList').html(NoticeHtml);
					// 初始化1次
					if (pageIndex == 1) {
                        Notice.showPage();
                    }
                } else {
                	alert(response.message);
                }
            }
		})
    },
	// 显示分页按钮
	showPage: function () {
		// 确保组件加载完成
        laypage.render({
            elem: 'pagination'
            ,count: Notice.data.totalData //数据总数，从服务端得到
			,limit: Notice.data.lineSize
            ,theme: '#337ab7' // 主题色
            ,jump: function(obj, first){
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                //首次不执行
                if(!first){
                    Notice.page(obj.curr);
                }
            }
        });
    },
    look: function() {
    	
    	      $("#mymodal").modal("toggle");
    	      var c = $(this).parent().attr('data-value');
    	      document.getElementById("modal-title").innerHTML = "公告内容";
    	      document.getElementById("modal-body").innerHTML = c;
    	    
    	//var c = $(this).parent().attr('data-value');
    	//alert('公告内容： \n' + c);
    }
}